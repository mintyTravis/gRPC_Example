#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <thread>
#include <fstream>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include "../protos/mud_objects.grpc.pb.h"


class ObjectClient{
    public:
        ObjectClient (std::shared_ptr<grpc::Channel> channel);
		enum obj_type_enum {UNKNOWN, LIGHT, SCROLL, WAND, STAFF, WEAPON, FIREWEAPON, MISSLE, TREASURE, ARMOR, 
						POTION, WORN, OTHER, TRASH, TRAP, CONTAINER, NOTE, DRINKCON, KEY, FOOD, MONEY,
						PEN, BOAT, FOUNTAIN, HERB, SKIN};
		std::fstream logfile;

        bool QueryForObject(objectsp::object_id send_obj_id, objectsp::object* return_object);
		bool QueryAllObjects(objectsp::objects* return_objects);
		bool QueryForObjectByType(objectsp::object_type get_object_type,objectsp::objects* return_objects);
		bool QueryForObjectByName(objectsp::object_name get_object_name, objectsp::objects* return_objects);
		bool EditObject(objectsp::object edit_object);
		bool DeleteObject(objectsp::object_id delete_id);
		bool CreateObject(objectsp::object new_object, objectsp::object_id* return_id);

		~ObjectClient(){};
		

    private:
        bool GetOneObject(objectsp::object_id& object_id, objectsp::object* return_object);
		bool GetAllObjects(objectsp::Empty& obj_empty, objectsp::objects* return_objects);
		bool GetObjectByType(objectsp::object_type& object_type, objectsp::objects* return_objects);
		bool GetObjectByName(objectsp::object_name& object_name, objectsp::objects* return_objects);
		bool EditOneObject(objectsp::object& edit_object, objectsp::Ack* edit_Ack);
		bool DeleteOneObject(objectsp::object_id& delete_id, objectsp::Ack* delete_ack);
		bool CreateOneObject(objectsp::object& new_object, objectsp::object_id* return_id);

        std::unique_ptr<objectsp::MUDCommunication::Stub> stub_;
};

bool checkStringExtra(std::string check_string);
int enum_convert(std::string obj_type);
void logger(std::string log_info,int option);