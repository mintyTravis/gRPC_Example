#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <thread>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include "../protos/mud_objects.grpc.pb.h"
#include "object_client.h"

//TODO: add new flag that will query by name and force to look for what was exaclty typed in.
//----  currently the -n or --name options will split by space and query the key words for either word that is present.
//TODO: add delete, edit, create and help commands.
//TODO: To make the object_client more portable - need to pass the objects back to main and have main parse the files to terminal.
//----  currently object_client.cpp handles this and would make it easier to use with other GUI/console code if it returns objects.


bool handleArgs(int argc, char* argv[], std::vector<std::string> &argvVector)
{
	bool badarg = true;
	std::string argv1Array[] = {"get","edit","delete","create","help"};
	std::string argv2Array[] = {"-i","--id","-a","--all","-t","--type","-n","--name"};

	for (int x = 1; x < argc; x++) {
		if (x == 1){
			for (auto y : argv1Array) {
				if (y == argv[x]) {
					argvVector.push_back(argv[x]);
					badarg = false;
					break;
				}
			}
		}
		else if (x == 2 && badarg == false) {
			badarg = true;
			if (argvVector[0] != "get"){
				argvVector.push_back(argv[x]);
				badarg = false;
				continue;
			}
			for(auto y : argv2Array) {
				if (y==argv[x]) {
					argvVector.push_back(argv[x]);
					badarg = false;
				}
			}
		}
		else if (x > 2 && badarg == false) {
			argvVector.push_back(argv[x]);
		}
	}
	return badarg;
}


int main (int argc, char* argv[])
{
	std::vector<std::string> argumentHandlerVector;

	//Check arguements passed through the terminal command.
	//The handleArgs function will fill the argumentHandlerVector with arguments we care about.
	//If we get an unexpected argument this will return true and we will exit the program.
	if (handleArgs(argc, argv, argumentHandlerVector)){
		std::cout<< "Received a bad command - please refer to the help command by typing 'client help'" << std::endl;
		return 0;
	}

	if (argumentHandlerVector[0] == "help" || argumentHandlerVector[0] == "--help") {
		std::cout << "Here is how you work the client: " << std::endl
				  << "Commands---" << std::endl
				  << "help - will display the help menu with a list of commands." << std::endl
				  << "get - will query for objects using the sup commands --- " << std::endl
				  << "      -i --id (looks for item by id and displays it to console)" << std::endl
				  << "      -t --type (looks for items by type and displays them to console)" << std::endl
				  << "      -n --name (looks for items by alias name (will match on word) and displays to console)" << std::endl
				  << "      -a --all (displays all items to console)" << std::endl
				  << "Proper use './object_client.o get [subcommand] [id/type/name]" << std::endl 
				  << "delete - will delete selected object: delete [object id]." << std::endl
				  << "create - will create an item and allow you to add each field." << std::endl;
	}

	//Once we get here we know we have commands we can act on. We open the channel to the server using insecure creds.
	ObjectClient objClient(grpc::CreateChannel("localhost:50051",grpc::InsecureChannelCredentials()));
//Starting get if nest---------------------------------------------------------------------------------	
//-----------------------------------------------------------------------------------------------------
	if (argumentHandlerVector[0] == "get") {
		if (argumentHandlerVector[1] == "-i" || argumentHandlerVector[1] == "--id"){
			objectsp::object_id send_obj_id;
			objectsp::object return_object;
			send_obj_id.set_id(stoi(argumentHandlerVector[2]));
			if(objClient.QueryForObject(send_obj_id,&return_object)) {

			std::cout << "Object found!" << std::endl
        			  << "Object Virtual Number: " << return_object.obj_id() << std::endl
        			  << "Object Alias List: " << return_object.obj_alias_list() << std::endl
        			  << "Object Short Description: " << return_object.obj_description_short() << std::endl
        			  << "Object Long Description: " << return_object.obj_description_long() << std::endl
        			  << "Object Action Description: " << return_object.obj_description_action() << std::endl
        			  << "Object Type Flag: " << return_object.obj_type_flag() << std::endl
        			  << "Object Effects BitVector: " << return_object.obj_effects_bv() << std::endl
        			  << "Object Wear BitVector: " << return_object.obj_wear_bv() << std::endl
        			  << "Object Value 0: " << return_object.obj_value0() << std::endl
        			  << "Object Value 1: " << return_object.obj_value1() << std::endl
        			  << "Object Value 2: " << return_object.obj_value2() << std::endl
        			  << "Object Value 3: " << return_object.obj_value3() << std::endl
        			  << "Object Value 4: " << return_object.obj_value4() << std::endl
        			  << "Object Object Weight: " << return_object.obj_weight() << std::endl
        			  << "Object Object Cost: " << return_object.obj_cost() << std::endl
        			  << "Object Object Rent: " << return_object.obj_rent_day() << std::endl;
    
        	//The following code is run to determine what will display for the extra description
        	//We loop through until there are no more values left (extra descriptions can recur many times and are marked with either...
        	//E or A
        	for (int x = 0; x < return_object.obj_extra_description_size();x++) {
        	    const objectsp::object::extra_description extraDescription = return_object.obj_extra_description(x);
        	    std::cout <<"Extra Description Type: " << extraDescription.obj_extra() << std::endl;
        	    if (extraDescription.obj_extra() == "E") {
        	        std::cout<< "Extra Description Keywords: " << extraDescription.obj_keyword_list() << std::endl;
        	        std::cout<< "Extra Description Extra: " << extraDescription.obj_description_extra() << std::endl;  
        	    } 
        	    else {
        	        std::cout<< "Extra Description Location: " << extraDescription.obj_location() << std::endl;
        	        std::cout<< "Extra Description Value: " << extraDescription.obj_value() << std::endl;
        	    }
        	}
        	std::cout << "End of Object: -------------------" << std::endl;
			return 0;
			}
			else
			{
				return 0;
			}
			
		}
		else if (argumentHandlerVector[1] == "-a" || argumentHandlerVector[1] == "--all") {
			std::cout<<"All selected. Ignoring other arguments..." <<std::endl;
			objectsp::objects return_objects;

			if (objClient.QueryAllObjects(&return_objects)) {
				std::cout<< "Starting Object List. Total of " << return_objects.object_size() << " objects." << std::endl;
				for (int x = 0; x < return_objects.object_size(); x++){
					objectsp::object return_object = return_objects.object(x);
        			std::cout << "Object Virtual Number: " << return_object.obj_id() << std::endl
        					  << "Object Alias List: " << return_object.obj_alias_list() << std::endl
        					  << "Object Short Description: " << return_object.obj_description_short() << std::endl
        					  << "Object Long Description: " << return_object.obj_description_long() << std::endl
        					  << "Object Action Description: " << return_object.obj_description_action() << std::endl
        					  << "Object Type Flag: " << return_object.obj_type_flag() << std::endl
        					  << "Object Effects BitVector: " << return_object.obj_effects_bv() << std::endl
        					  << "Object Wear BitVector: " << return_object.obj_wear_bv() << std::endl
        					  << "Object Value 0: " << return_object.obj_value0() << std::endl
        					  << "Object Value 1: " << return_object.obj_value1() << std::endl
        					  << "Object Value 2: " << return_object.obj_value2() << std::endl
        					  << "Object Value 3: " << return_object.obj_value3() << std::endl
        					  << "Object Value 4: " << return_object.obj_value4() << std::endl
        					  << "Object Object Weight: " << return_object.obj_weight() << std::endl
        					  << "Object Object Cost: " << return_object.obj_cost() << std::endl
        					  << "Object Object Rent: " << return_object.obj_rent_day() << std::endl;
					for (int x = 0; x < return_object.obj_extra_description_size();x++) {
            			const objectsp::object::extra_description extraDescription = return_object.obj_extra_description(x);
            			std::cout <<"Extra Description Type: " << extraDescription.obj_extra() << std::endl;
            			if (extraDescription.obj_extra() == "E") {
            			    std::cout<< "Extra Description Keywords: " << extraDescription.obj_keyword_list() << std::endl;
            			    std::cout<< "Extra Description Extra: " << extraDescription.obj_description_extra() << std::endl;  
            			} 
            			else {
            			    std::cout<< "Extra Description Location: " << extraDescription.obj_location() << std::endl;
            			    std::cout<< "Extra Description Value: " << extraDescription.obj_value() << std::endl;
            			}
       			 	}
       			std::cout << "End of Object: -------------------" << std::endl;
				}
				return 0;
			}
			else {
				return 0;
			}
		}//End if "-a"
		else if (argumentHandlerVector[1] == "-t" || argumentHandlerVector[1] == "--type") {
			objectsp::object_type get_object_type;
			objectsp::objects return_objects;
			get_object_type.set_type(enum_convert(argumentHandlerVector[2]));
			if(objClient.QueryForObjectByType(get_object_type, &return_objects)){

			std::cout<< "Starting Object List. Total of " << return_objects.object_size() << " objects." << std::endl;
			for (int x = 0; x < return_objects.object_size(); x++){

				objectsp::object return_object = return_objects.object(x);
        		std::cout << "Object Virtual Number: " << return_object.obj_id() << std::endl
        				  << "Object Alias List: " << return_object.obj_alias_list() << std::endl
        				  << "Object Short Description: " << return_object.obj_description_short() << std::endl
        				  << "Object Long Description: " << return_object.obj_description_long() << std::endl
        				  << "Object Action Description: " << return_object.obj_description_action() << std::endl
        				  << "Object Type Flag: " << return_object.obj_type_flag() << std::endl
        				  << "Object Effects BitVector: " << return_object.obj_effects_bv() << std::endl
        				  << "Object Wear BitVector: " << return_object.obj_wear_bv() << std::endl
        				  << "Object Value 0: " << return_object.obj_value0() << std::endl
        				  << "Object Value 1: " << return_object.obj_value1() << std::endl
        				  << "Object Value 2: " << return_object.obj_value2() << std::endl
        				  << "Object Value 3: " << return_object.obj_value3() << std::endl
        				  << "Object Value 4: " << return_object.obj_value4() << std::endl
        				  << "Object Object Weight: " << return_object.obj_weight() << std::endl
        				  << "Object Object Cost: " << return_object.obj_cost() << std::endl
        				  << "Object Object Rent: " << return_object.obj_rent_day() << std::endl;

				for (int x = 0; x < return_object.obj_extra_description_size();x++) {
        	    	const objectsp::object::extra_description extraDescription = return_object.obj_extra_description(x);
        	    	std::cout <<"Extra Description Type: " << extraDescription.obj_extra() << std::endl;

        	    	if (extraDescription.obj_extra() == "E") {
        	    	    std::cout<< "Extra Description Keywords: " << extraDescription.obj_keyword_list() << std::endl;
        	    	    std::cout<< "Extra Description Extra: " << extraDescription.obj_description_extra() << std::endl;  
        	    	} 
        	    	else {
        	    	    std::cout<< "Extra Description Location: " << extraDescription.obj_location() << std::endl;
        	    	    std::cout<< "Extra Description Value: " << extraDescription.obj_value() << std::endl;
        	    	}
				}//end extra description for loop
			}//end objects for loop
        	std::cout << "End of Object: -------------------" << std::endl;
			return 0;
			}
			else{
				std::cout << "Error trying to get objects by type, review logs" <<std::endl;
				return 0;
			}
		}//end if --type / -t
		else if (argumentHandlerVector[1] == "-n" || argumentHandlerVector[1] == "--name") {
				std::string obj_name;
				objectsp::object_name get_obj_name;
				objectsp::objects return_objects;

				for(int x = 2; x < argumentHandlerVector.size(); x++) {
					for (int y = 0; y < argumentHandlerVector[x].size(); y++){
						obj_name.push_back(argumentHandlerVector[x][y]);
					}
					if (x+1 < argumentHandlerVector.size()) obj_name.push_back(' ');
				}
				get_obj_name.set_obj_name(obj_name);

				if(objClient.QueryForObjectByName(get_obj_name,&return_objects)){
					std::cout<< "Starting Object List......." << std::endl;

					for (int x = 0; x < return_objects.object_size(); x++){
						objectsp::object return_object = return_objects.object(x);
        				std::cout << "Object Virtual Number: " << return_object.obj_id() << std::endl
        						  << "Object Alias List: " << return_object.obj_alias_list() << std::endl
        						  << "Object Short Description: " << return_object.obj_description_short() << std::endl
        						  << "Object Long Description: " << return_object.obj_description_long() << std::endl
        						  << "Object Action Description: " << return_object.obj_description_action() << std::endl
        						  << "Object Type Flag: " << return_object.obj_type_flag() << std::endl
        						  << "Object Effects BitVector: " << return_object.obj_effects_bv() << std::endl
        						  << "Object Wear BitVector: " << return_object.obj_wear_bv() << std::endl
        						  << "Object Value 0: " << return_object.obj_value0() << std::endl
        						  << "Object Value 1: " << return_object.obj_value1() << std::endl
        						  << "Object Value 2: " << return_object.obj_value2() << std::endl
        						  << "Object Value 3: " << return_object.obj_value3() << std::endl
        						  << "Object Value 4: " << return_object.obj_value4() << std::endl
        						  << "Object Object Weight: " << return_object.obj_weight() << std::endl
        						  << "Object Object Cost: " << return_object.obj_cost() << std::endl
        						  << "Object Object Rent: " << return_object.obj_rent_day() << std::endl;

						for (int x = 0; x < return_object.obj_extra_description_size();x++) {
        			    	const objectsp::object::extra_description extraDescription = return_object.obj_extra_description(x);
        			    	std::cout <<"Extra Description Type: " << extraDescription.obj_extra() << std::endl;

        			    	if (extraDescription.obj_extra() == "E") {
        			    	    std::cout<< "Extra Description Keywords: " << extraDescription.obj_keyword_list() << std::endl;
        			    	    std::cout<< "Extra Description Extra: " << extraDescription.obj_description_extra() << std::endl;  
        			    	} 
        			    	else {
        			    	    std::cout<< "Extra Description Location: " << extraDescription.obj_location() << std::endl;
        			    	    std::cout<< "Extra Description Value: " << extraDescription.obj_value() << std::endl;
        			    	}
        				}//end extra description for loop
        				std::cout << "End of Object: -------------------" << std::endl;
					}//end objects for loop	
					std::cout << "Completed list, there was a total of " << return_objects.object_size() << " objects." << std::endl;	
				}//
				else{
					return 0;
				}
		}//end if -n // --name
		else {
			std::cout << "Please specify the type of 'get' {--id [object id], --all, --type [object type], --name [object name]" << std::endl;
			return 0;
		}
	}
//----------------------------
	if (argumentHandlerVector[0] == "edit") {
		if (argc < 3) {
			std::cout << "Missing command - please review help text" <<std::endl;
			return 0;
		}
		std::cout << "Edit selected: Querying object...." << std::endl;
		objectsp::object_id edit_object_id;
		objectsp::object return_object;
		edit_object_id.set_id(stoi(argumentHandlerVector[1]));
		if (objClient.QueryForObject(edit_object_id, &return_object)){
			objectsp::object send_object;
			send_object.CopyFrom(return_object);
			int userchoice1 = 0, userchoice2 = 0, lastchoice = 0;
			std::string userstring;
			bool userdone = false;

			for (int x = 0; userdone == false; x++) {
				std::cout <<"Which item would you like to edit? " << std::endl
						  << "1. Alias: " << send_object.obj_alias_list() << std::endl
						  << "2. Short Description: " << send_object.obj_description_short() << std::endl
						  << "3. Long Description: " << send_object.obj_description_long() << std::endl
						  << "4. Action Description: " << send_object.obj_description_action() << std::endl
						  << "5. Type Flag: " << send_object.obj_type_flag() << std::endl
						  << "6. Effects BitVector: " << send_object.obj_effects_bv() << std::endl
						  << "7. Wear BitVector: " << send_object.obj_wear_bv() << std::endl
						  << "8. Value 0: " << send_object.obj_value0() << std::endl
						  << "9. Value 1: " << send_object.obj_value1() << std::endl
						  << "10. Value 2: " << send_object.obj_value2() << std::endl
						  << "11. Value 3: " << send_object.obj_value3() << std::endl
						  << "12. Value 4: " << send_object.obj_value4() << std::endl
						  << "13. Weight: " << send_object.obj_weight() << std::endl
						  << "14. Cost: " << send_object.obj_cost() << std::endl
						  << "15. Rent: " << send_object.obj_rent_day() << std::endl
						  << "16. Extra Description 'E' or... " << std::endl
						  //Can either give a secondary option to modify one extra description repeating field, or force them to enter everything again :).
						  << "17. Extra Description 'A'-------- " << std::endl;
						  for (int x = 0; x < send_object.obj_extra_description_size();x++) {
        				      const objectsp::object::extra_description extraDescription = send_object.obj_extra_description(x);
        				      std::cout <<"Extra Description Type: " << extraDescription.obj_extra() << std::endl;
        				      if (extraDescription.obj_extra() == "E") {
        				          std::cout<< "Extra Description Keywords: " << extraDescription.obj_keyword_list() << std::endl;
        				          std::cout<< "Extra Description Extra: " << extraDescription.obj_description_extra() << std::endl;  
        				      } 
        				      else {
        				          std::cout<< "Extra Description Location: " << extraDescription.obj_location() << std::endl;
        				          std::cout<< "Extra Description Value: " << extraDescription.obj_value() << std::endl;
        				      }
        				  }
				std::cout << "18. Complete" << std::endl
						  << "19. Cancel" << std::endl
						  << "Enter the number! ";

				std::cin >> userchoice1;

				switch (userchoice1){
					case 1:
						std::cout << "Enter new  alias list: ";
						std::cin.ignore();
						//std::cin.clear(); std::cin.sync();
						std::getline(std::cin,userstring);
						send_object.set_obj_alias_list(userstring);
						break;
					case 2:
						std::cout <<"Enter new short description: ";
						std::cin.ignore();
						std::getline(std::cin,userstring);
						send_object.set_obj_description_short(userstring);
						break;
					case 3:
						std::cout <<"Enter new long description: ";
						std::cin.ignore();
						std::getline(std::cin,userstring);
						send_object.set_obj_description_long(userstring);
						break;
					case 4:
						std::cout <<"Enter new action description: ";
						std::cin.ignore();
						std::getline(std::cin,userstring);
						send_object.set_obj_description_action(userstring);
						break;
					case 5:
						std::cout <<"Enter new type flag: ";
						std::cin >> userchoice2;
						send_object.set_obj_type_flag(userchoice2);
						break;
					case 6:
						std::cout <<"Enter new effects bitvector: ";
						std::cin >> userchoice2;
						send_object.set_obj_effects_bv(userchoice2);
						break;
					case 7:
						std::cout <<"Enter new wear bitvector: ";
						std::cin >> userchoice2;
						send_object.set_obj_wear_bv(userchoice2);
						break;
					case 8:
						std::cout <<"Enter new value 0: ";
						std::cin >> userchoice2;
						send_object.set_obj_value0(userchoice2);
						break;
					case 9:
						std::cout <<"Enter new value 1: ";
						std::cin >> userchoice2;
						send_object.set_obj_value1(userchoice2);
						break;
					case 10:
						std::cout <<"Enter new value 2: ";
						std::cin >> userchoice2;
						send_object.set_obj_value2(userchoice2);
						break;
					case 11:
						std::cout <<"Enter new value 3: ";
						std::cin >> userchoice2;
						send_object.set_obj_value3(userchoice2);
						break;
					case 12:
						std::cout <<"Enter new value 4: ";
						std::cin >> userchoice2;
						send_object.set_obj_value4(userchoice2);
						break;
					case 13:
						std::cout <<"Enter new weight: ";
						std::cin >> userchoice2;
						send_object.set_obj_weight(userchoice2);
						break;
					case 14:
						std::cout <<"Enter new cost: ";
						std::cin >> userchoice2;
						send_object.set_obj_cost(userchoice2);
						break;
					case 15:
						std::cout <<"Enter new rent: ";
						std::cin >> userchoice2;
						send_object.set_obj_rent_day(userchoice2);
						break;
					case 16:
						std::cout <<"Enter new Extra description E: " << std::endl;
						std::cin.ignore();
						//TODO: add method of getting new extra description here.
						break;
					case 17:
						std::cout <<"Enter new Extra description A: ";
						//TODO: add method of getting new action description here.
					case 18:
						userdone = true;
						if (objClient.EditObject(send_object)){
							std::cout << "Successfully edited the object - use the get function to see the new edit." << std::endl;
							return 0;
						}
						else {
							std::cout << "Object Edit failed - review logs to see what happened." << std::endl;
							return 0;
						}						
						break;
					default:
						std::cout << "Option not accepted: try again." << std::endl;
						break;
				}// end switch
			}// end for loop
		} // end if
		else {
				std::cout << "Unable to find object";
				return 0;
		}
		return 0;
	}//end edit if

	if (argumentHandlerVector[0] == "delete") {
		if (argc < 3){
			std::cout << "Missing argument, need object id" << std::endl;
			return 0;
		}
		std::cout << "Delete selected: Querying object...." << std::endl;
		objectsp::object_id delete_id;

		delete_id.set_id(stoi(argumentHandlerVector[1]));
		if(objClient.DeleteObject(delete_id)){
			std::cout << "Successfully deleted object: " << delete_id.id() << std::endl;
			return 0;
		}
		else {
			std::cout << "Something went wrong! Check the log file" << std::endl;
			return 0;
		}
	}//end delete if

	if (argumentHandlerVector[0] == "create") {
		std::string user_input_string;
		char user_input_extra_description = ' ';
		int user_input_int;
		objectsp::object send_object;
		objectsp::object_id return_id;

		std::cout << "Create new object selected: " << std::endl;
		std::cout <<"Please enter the following object information: " << std::endl
				  << "1. Alias- " << std::endl;
				  std::getline(std::cin,user_input_string);
				  send_object.set_obj_alias_list(user_input_string);
		std::cout << "2. Short Description- " << std::endl;
				  std::getline(std::cin,user_input_string);
				  send_object.set_obj_description_short(user_input_string);
    	std::cout << "3. Long Description: " << std::endl;
				  std::getline(std::cin,user_input_string);
				  send_object.set_obj_description_long(user_input_string);
		std::cout << "4. Action Description: " << std::endl;
				  std::getline(std::cin,user_input_string);
				  send_object.set_obj_description_action(user_input_string);
		std::cout << "5. Type Flag: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_type_flag(user_input_int);
		std::cout << "6. Effects BitVector: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_effects_bv(user_input_int);
		std::cout << "7. Wear BitVector: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_wear_bv(user_input_int);
		std::cout << "8. Value 0: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_value0(user_input_int);
		std::cout << "9. Value 1: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_value1(user_input_int);
		std::cout << "10. Value 2: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_value2(user_input_int);
		std::cout << "11. Value 3: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_value3(user_input_int);
		std::cout << "12. Value 4: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_value4(user_input_int);
		std::cout << "13. Weight: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_weight(user_input_int);
		std::cout << "14. Cost: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_cost(user_input_int);
		std::cout << "15. Rent: " << std::endl;
				  std::cin >> user_input_int;
				  send_object.set_obj_rent_day(user_input_int);
		for (int y = 0; user_input_extra_description != 'C' || user_input_extra_description != 'c'; y++) {
			std::cout << "16. Extra Description 'E' or 'A' (type 'C' to stop adding extra descriptions): " << std::endl;
					  std::cin >> user_input_extra_description;
					  user_input_extra_description = toupper(user_input_extra_description);			  	  
					  if (user_input_extra_description == 'C') {
						break;
					  }
			if (user_input_extra_description == 'E' || user_input_extra_description == 'A') {
					  	objectsp::object_extra_description* add_extra_desc = send_object.add_obj_extra_description();
					  	add_extra_desc->set_obj_extra(&user_input_extra_description);
				if (user_input_extra_description == 'A') {
					std::cout << "1. Extra Description Location: ";
				  			  std::cin >> user_input_int;
				  			  add_extra_desc->set_obj_location(user_input_int);
					std::cout << "2. Extra Description Value: ";
				  			  std::cin >> user_input_int;
							  add_extra_desc->set_obj_value(user_input_int);
				}
				else if(user_input_extra_description == 'E' || user_input_extra_description == 'e') {
					std::cout << "1. Extra Description Keywords- " << std::endl;
				  			  std::cin.ignore();
							  std::getline(std::cin,user_input_string);
							  add_extra_desc->set_obj_keyword_list(user_input_string);
					std::cout << "2. Extra Description Long Description - (use \\n[enter] to enter string on multiple lines)\n";
							  std::getline(std::cin,user_input_string);
							  for(int x = 0; checkStringExtra(user_input_string);x++){
								  user_input_string.replace(user_input_string.find('\\'),user_input_string.size(),1,'\n');
								  add_extra_desc->set_obj_description_extra(add_extra_desc->obj_description_extra() + user_input_string);
								  std::cout<< "Extra string-\n" << add_extra_desc->obj_description_extra() << std::endl;
								  std::getline(std::cin,user_input_string);
							  }
							  add_extra_desc->set_obj_description_extra(add_extra_desc->obj_description_extra() + user_input_string);
				}
				else {
					std::cout << "Entered invalid input - try again..." << std::endl;
					user_input_extra_description = ' ';
				}
			}
		}//end for loop
 		std::cout<< "1. Alias: " << send_object.obj_alias_list() << std::endl
				  << "2. Short Description: " << send_object.obj_description_short() << std::endl
				  << "3. Long Description: " << send_object.obj_description_long() << std::endl
				  << "4. Action Description: " << send_object.obj_description_action() << std::endl
				  << "5. Type Flag: " << send_object.obj_type_flag() << std::endl
				  << "6. Effects BitVector: " << send_object.obj_effects_bv() << std::endl
				  << "7. Wear BitVector: " << send_object.obj_wear_bv() << std::endl
				  << "8. Value 0: " << send_object.obj_value0() << std::endl
				  << "9. Value 1: " << send_object.obj_value1() << std::endl
				  << "10. Value 2: " << send_object.obj_value2() << std::endl
				  << "11. Value 3: " << send_object.obj_value3() << std::endl
				  << "12. Value 4: " << send_object.obj_value4() << std::endl
				  << "13. Weight: " << send_object.obj_weight() << std::endl
				  << "14. Cost: " << send_object.obj_cost() << std::endl
				  << "15. Rent: " << send_object.obj_rent_day() << std::endl
				  << "16. Extra Description 'E' or... " << std::endl
				  << "17. Extra Description 'A'-------- " << std::endl;
				  for (int x = 0; x < send_object.obj_extra_description_size();x++) {
    	    	      const objectsp::object::extra_description extraDescription = send_object.obj_extra_description(x);
    	    	      std::cout <<"Extra Description Type: " << extraDescription.obj_extra() << std::endl;
    	    	      if (extraDescription.obj_extra() == "E") {
    	    	          std::cout<< "Extra Description Keywords: " << extraDescription.obj_keyword_list() << std::endl;
    	    	          std::cout<< "Extra Description Extra: " << extraDescription.obj_description_extra() << std::endl;  
    	    	      } 
    	    	      else {
    	    	          std::cout<< "Extra Description Location: " << extraDescription.obj_location() << std::endl;
    	    	          std::cout<< "Extra Description Value: " << extraDescription.obj_value() << std::endl;
    	    	      }
    	    	  }
		if(objClient.CreateObject(send_object,&return_id)){
			std::cout<< "New object id: " << return_id.id() << std::endl;
			return 0;
		}
		else
		{
			return 0;
		}
		
	}// end create if

	return 0;
}