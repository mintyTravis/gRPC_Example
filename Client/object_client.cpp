#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <thread>
#include <fstream>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include "../protos/mud_objects.grpc.pb.h"
#include "object_client.h"

/**
 * Simple Logging function
 * @param1 log_info - string
 * @param2 option - options (int)
 * 
 * Takes a string and will log the information into Client/Logs/log.txt
 * Options are -----
 * 0 - Informational
 * 1 - Warning
 * 2 - Error
 */ 
void logger(std::string log_info,int option = 0)
{
	std::ofstream logfile;
	std::time_t time_now;
	time_now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::string time_now_str = std::ctime(&time_now);
	time_now_str.pop_back();

	logfile.open("Logs/log.txt",std::ios::app);

	if (option == 0) {
		logfile << "INFO: " << time_now_str << " " << log_info << std::endl;
	}
	else if (option == 1) {
		logfile << "WARNING: " << time_now_str << " " << log_info << std::endl;
	}
	else if (option == 2) {
		logfile << "ERROR: " << time_now_str << " " << log_info << std::endl;
	}
	else {
		logfile << "INFO (INVALID PARAM): " << time_now_str << " " << log_info << std::endl;
	}
}

/**
 * @Param1 obj_type - string
 * 
 * This method takes the string from the executable argument and converts it to
 * and int based on the corresponding object type. 
 * 
 */
int enum_convert(std::string obj_type)
{
	std::transform(obj_type.begin(), obj_type.end(), obj_type.begin(), toupper);

	if (obj_type == "LIGHT") return(ObjectClient::LIGHT);
	if (obj_type == "SCROLL") return(ObjectClient::SCROLL);
	if (obj_type == "WAND") return(ObjectClient::WAND);
	if (obj_type == "STAFF") return(ObjectClient::STAFF);
	if (obj_type == "WEAPON") return(ObjectClient::WEAPON);
	if (obj_type == "MISSLE") return(ObjectClient::MISSLE);
	if (obj_type == "TREASURE") return(ObjectClient::TREASURE);
	if (obj_type == "ARMOR") return(ObjectClient::ARMOR);
	if (obj_type == "POTION") return(ObjectClient::POTION);
	if (obj_type == "WORN") return(ObjectClient::WORN);
	if (obj_type == "OTHER") return(ObjectClient::OTHER);
	if (obj_type == "TRASH") return(ObjectClient::TRASH);
	if (obj_type == "TRAP") return(ObjectClient::TRAP);
	if (obj_type == "CONTAINER") return(ObjectClient::CONTAINER);
	if (obj_type == "NOTE") return(ObjectClient::NOTE);
	if (obj_type == "DRINKCON") return(ObjectClient::DRINKCON);
	if (obj_type == "KEY") return(ObjectClient::KEY);
	if (obj_type == "FOOD") return(ObjectClient::FOOD);
	if (obj_type == "MONEY") return(ObjectClient::MONEY);
	if (obj_type == "PEN") return(ObjectClient::PEN);
	if (obj_type == "BOAT") return(ObjectClient::BOAT);
	if (obj_type == "FOUNTAIN") return(ObjectClient::FOUNTAIN);
	if (obj_type == "HERB") return(ObjectClient::HERB);
	if (obj_type == "SKIN") return(ObjectClient::SKIN);

	try
	{
		int return_type_obj = stoi(obj_type);
		return return_type_obj;
	}
	catch(const std::exception& e)
	{
		std::cout <<"Error at enum convert: " << e.what() << std::endl;
		logger("Failer at enum convert",2);
		return ObjectClient::UNKNOWN;
	}
}

/**
 * Check Extra String
 * @param1 check_string - string type
 * 
 * This checks the string for a \\n at the end and used for the edit/create methods
 * This will allow the user to add new lines to the extra description strings via terminal UI.
 */
bool checkStringExtra(std::string check_string) {
	std::string check_string2;
	check_string2.replace(0,2,check_string,check_string.size()-2,2);
	if(check_string2 == "\\n") return true;
	else return false;
}

// Default constructor. Opens channel with server.
ObjectClient::ObjectClient (std::shared_ptr<grpc::Channel> channel) : stub_(objectsp::MUDCommunication::NewStub(channel)) {

}

/**
 * @param1 send_obj_id - object_id message type from proto file
 * @param2 return_object - object message type from proto file
 * 
 * This method will take the object ID from executable and return
 * the single object if the server finds it. If not returns false.
 */ 
bool ObjectClient::QueryForObject(objectsp::object_id send_obj_id, objectsp::object* return_object) {
    if (GetOneObject(send_obj_id,return_object)) {
		logger("Successfully called QueryForObject",0);
		return true;
	}
	else return false;
}

/**
 * @Param1 return_objects - objects type message from proto file
 * 
 * This method will take no input, then send the empty message to the server
 * and return all objects in return_objects.
 */ 
bool ObjectClient::QueryAllObjects(objectsp::objects* return_objects){
	objectsp::Empty obj_empty;

	if(GetAllObjects(obj_empty,return_objects)){
		logger("Successfully called GetAllObjects");
		return true;
	}
	else {
		return false;
	}
}

/**
 * @Param1 get_object_type - object type message from proto file
 * @Param2 return_objects - objects type message proto file
 * 
 * This method will receive the object type int, and a blank return objects
 * Using the object type - it will send to the server to search for all objects
 * of that type and return them in return_objects.
 * 
 */
bool ObjectClient::QueryForObjectByType(objectsp::object_type get_object_type,objectsp::objects* return_objects) {
	if (GetObjectByType(get_object_type,return_objects)) {
		logger("Successfully called GetObjectByType");
		return true;
	}
	else {
		return false;
	}	
}

/**
 * @param1 get_object_name - object name message type from proto file
 * @param2 return_objects - objects message type from proto file
 * 
 * This method will be called with get -n or --name, then take each word after
 * and return objects that have the corresponding word in their alias.
 *  
 */
bool ObjectClient::QueryForObjectByName(objectsp::object_name get_object_name, objectsp::objects* return_objects) {
	if (GetObjectByName(get_object_name,return_objects)){
		logger("Successfully callsed GetObjectByName");
		return true;
	}
	else {
		return false;
	}
}

/**
 * @param1 edit_object - object type from proto file
 * 
 * This method will take an edited object and send it to the server.
 * The server will update the object and return an acknowledgement of true or false.
 * 
 */
bool ObjectClient::EditObject(objectsp::object edit_object) {
	objectsp::Ack return_ack;
	if(EditOneObject(edit_object,&return_ack)){
		logger("Successfully called EditOneObject");
		return return_ack.acknowledge();
	}
	else{
		return false;
	}
}

bool ObjectClient::CreateObject(objectsp::object new_object, objectsp::object_id* return_id) {
	
	if(CreateOneObject(new_object,return_id)) {
		logger("Successfully called CreateOneObject!");
		return true;
	}
	else {
		return false;
	}

}

bool ObjectClient::DeleteObject(objectsp::object_id delete_id){
	objectsp::object return_object;
	objectsp::Ack send_ack;

	if(GetOneObject(delete_id,&return_object)){
		logger("Successfully called GetOneObject");
		if (DeleteOneObject(delete_id,&send_ack)) {
			logger("Successfully called DeleteOneObject");
			return send_ack.acknowledge();
		}
		else {
			return false;
		}
		
	}
}

//Private method used to get one object from server by object ID
bool ObjectClient::GetOneObject(objectsp::object_id& object_id, objectsp::object* return_object) {
    grpc::ClientContext context;
    grpc::Status status =stub_->QueryForObject(&context, object_id, return_object);

    if (!status.ok()) {
        std::cout << "Failed to query object" << std::endl;
		logger("Failed to query object",2);
		return false;
    }
    if (return_object->obj_id() == 0) {
        std::cout << "Object does not exist" << std::endl;
		std::string send_to_logger = "Object [" + object_id.id();
		send_to_logger = send_to_logger + "] does not exist";

		logger(send_to_logger,1);
		return false;
    }
    return true;       
}

//Private method used to get all objects from server
bool ObjectClient::GetAllObjects(objectsp::Empty& obj_empty, objectsp::objects* return_objects){
	grpc::ClientContext context;
	grpc::Status status = stub_->QueryAllObjects(&context,obj_empty,return_objects);

	if (!status.ok()){
		std::cout << "Failed to get back okay status from QueryAllObjects!" <<std::endl;
		logger("Failed to get back an ok response from server",2);
		return false;
	}
	else if (return_objects->object_size() <= 1) {
		std::cout << "Failed to get objects into return_objects!" << std::endl;
		logger("Failed to get all objects into the return objects",1);
		return false;
	}
	else {
		return true;
	}
}

//Private used method to send object type to server and return objects
bool ObjectClient::GetObjectByType(objectsp::object_type& object_type, objectsp::objects* return_objects) {
	grpc::ClientContext context;
	grpc::Status status = stub_->QueryForObjectByType(&context,object_type,return_objects);

	if (!status.ok()){
		std::cout << "Failed to get back okay status from QueryForObjectByType!" <<std::endl;
		logger("Failed to get okay status from GetObjectByType",2);
		return false;
	}
	else if (return_objects->object_size() <= 1) {
		std::cout << "Failed to get objects into return_objects!" << std::endl;
		logger("Received OK back from the server but return objects had no size",1);
		return false;
	}
	else {
		return true;
	}	
}

bool ObjectClient::GetObjectByName(objectsp::object_name& object_name, objectsp::objects* return_objects) {
	grpc::ClientContext context;
	grpc::Status status = stub_->QueryForObjectNames(&context,object_name,return_objects);

	if (!status.ok()){
		std::cout << "Failed to get back okay status from QueryForObjectNames!" <<std::endl;
		logger("Failed to get OK status from server at GetObjectByName",2);
		return false;
	}
	else if (return_objects->object_size() <= 0) {
		std::cout << "Failed to get objects into return_objects!" << std::endl;
		logger("Failed to receive any objects in return objects from GetObjectByName",1);
		return false;
	}
	else {
		return true;
	}
}

bool ObjectClient::EditOneObject(objectsp::object& edit_object, objectsp::Ack* edit_Ack){
	grpc::ClientContext context;
	grpc::Status status = stub_->EditObject(&context,edit_object,edit_Ack);

	if(!status.ok()) {
		std::cout << "Unable to call EditObject" << std::endl;
		logger("Failed to get OK status from server at EditOneObject",2);
		return false;
	}
	return true;
}

bool ObjectClient::DeleteOneObject(objectsp::object_id& delete_id,objectsp::Ack* delete_ack) {
	grpc::ClientContext context;
	grpc::Status status = stub_->DeleteObject(&context,delete_id,delete_ack);
	std::string log_string;

	if (!status.ok()) {
		logger("Failed to get OK status at DeleteOneObject",2);
		return false;
	}
	if (delete_ack->acknowledge() == true) {
		log_string = "Successfully deleted object: " + delete_ack->acknowledge();
		logger(log_string);
		return true;
	}
	else {
		return false;
	}
}

bool ObjectClient::CreateOneObject(objectsp::object& new_object, objectsp::object_id* return_id) {
	grpc::ClientContext context;
	grpc::Status status = stub_->CreateObject(&context,new_object,return_id);

	if(!status.ok()) {
		std::cout << "Failed to call CreateObject" << std::endl;
		logger("Failed to get OK status from server at CreateOneObject",2);
		return false;
	}
	else if (return_id->id() > 1) {
		return true;
	}
	else {
		logger("Uknown error at CreateOneObject",1);
		return false;
	}
}