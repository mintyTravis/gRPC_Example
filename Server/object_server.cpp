#include <iostream>
#include <fstream>
#include <string>
#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>
#include "object_server.h"
#include "../protos/mud_objects.grpc.pb.h"
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>

/**
 * Simple Logging function
 * @param1 log_info - string
 * @param2 option - options (int)
 * 
 * Takes a string and will log the information into Client/Logs/log.txt
 * Options are -----
 * 0 - Informational
 * 1 - Warning
 * 2 - Error
 */ 
void logger(std::string log_info,int option = 0)
{
	std::ofstream logfile;
	std::time_t time_now;
	time_now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::string time_now_str = std::ctime(&time_now);
	time_now_str.pop_back();

	logfile.open("Logs/log.txt",std::ios::app);

	if (option == 0) {
		logfile << "INFO: " << time_now_str << " " << log_info << std::endl;
	}
	else if (option == 1) {
		logfile << "WARNING: " << time_now_str << " " << log_info << std::endl;
	}
	else if (option == 2) {
		logfile << "ERROR: " << time_now_str << " " << log_info << std::endl;
	}
	else {
		logfile << "INFO (INVALID PARAM): " << time_now_str << " " << log_info << std::endl;
	}
}

objectsp::objects parseProtobuff(const std::string& db) {
    objectsp::objects return_objects;
    std::fstream inputfile(db,std::ios::in|std::ios::binary);
    if(!return_objects.ParseFromIstream(&inputfile)) {
        std::cout << "Failed to parse file at parseProtobuff" << std::endl;
		logger("Failed to parse file at parseProtobuf",2);
    }
	else {
		return_objects.DiscardUnknownFields();
	}
	
    return return_objects;
}

ObjectConnectionImpl::ObjectConnectionImpl(const std::string& db) {
    objects_file = parseProtobuff(db);
	database_ = db;
}

grpc::Status ObjectConnectionImpl::QueryForObject(grpc::ServerContext* context, 
                                                  const objectsp::object_id* object_id,objectsp::object* return_object) {
    std::cout <<"Successfully called QueryForObject-------" << std::endl;
	logger("Successfully called QueryForObject");
    bool ismatch = false;
    for (int x = 0; x < objects_file.object_size(); x++) {
        if (objects_file.object(x).obj_id() == object_id->id()) {
            ismatch = true;
            return_object->CopyFrom(objects_file.object(x));
            break;
        }
    }
    if (ismatch) {
        std::cout<< "We have a match! " << std::endl;
        return grpc::Status::OK;
    }
    else {
		return_object->set_obj_id(0);
        return grpc::Status::CANCELLED;
    }
}

grpc::Status ObjectConnectionImpl::QueryAllObjects(grpc::ServerContext* context,
													const objectsp::Empty* empty_obj,objectsp::objects* return_objects) {
	std::cout <<"Successfully called QueryAllObjects!" << std::endl;
	logger("Successfully called QueryForAllObjects");
	return_objects->CopyFrom(objects_file);
	return grpc::Status::OK;
}

grpc::Status ObjectConnectionImpl::QueryForObjectByType(grpc::ServerContext* context, const objectsp::object_type* objectType, objectsp::objects* return_objects) {
	std::cout<<"Successfully called QueryForObjectByType!" << std::endl;
	logger("Successfully called QueryForObjectByType");
	for (int x = 0; x < objects_file.object_size(); x++) {
		objectsp::object obj_type_check_object = objects_file.object(x);
		if (obj_type_check_object.obj_type_flag() == objectType->type()) {
			objectsp::object* copy_obj = return_objects->add_object();
			copy_obj->CopyFrom(obj_type_check_object);
		}		
	}
	return grpc::Status::OK;
}

grpc::Status ObjectConnectionImpl::QueryForObjectNames(grpc::ServerContext* context, const objectsp::object_name* objectName, objectsp::objects* return_objects) {
	std::cout<<"Successfully called QueryForObjectByName!" << std::endl;
	logger("Successfully called QueryForObjectByName");
	std::vector<std::string> query_each_word_string, query_each_word_obj;
	boost::split(query_each_word_string, objectName->obj_name(), [](char c){return c == ' ';});
	objectsp::object obj_name_;

	for(std::string x : query_each_word_string) {
		for(int y = 0; y < objects_file.object_size(); y++) {
			obj_name_ = objects_file.object(y);
			boost::split(query_each_word_obj,obj_name_.obj_alias_list(), [](char c){return c == ' ';});
			for(std::string z : query_each_word_obj) {
				if (x == z) {
					objectsp::object* copy_obj = return_objects->add_object();
					copy_obj->CopyFrom(obj_name_);
					break;
				}
			}

		}
	}
	return grpc::Status::OK;
}

grpc::Status ObjectConnectionImpl::EditObject(grpc::ServerContext* context, const objectsp::object* edit_object, objectsp::Ack* edit_ack) {
	//objectsp::objects new_objects_file;
	std::ofstream outfile;
	std::string log_string;
	logger("Successfully called EditObject method");

	std::cout << "Succesfully called 'EditObject' method.\n "	
			  << "Attempting to edit object id: " << edit_object->obj_id() << std::endl;
			  log_string = "Attempting to edit object id: " + edit_object->obj_id();
			  logger(log_string);
	outfile.open(database_, std::ios::out|std::ios::binary);
	if(outfile.good()){
		outfile.clear();
		std::cout << "Opened file - "<< database_ <<std::endl;
		for(int x = 0; x < objects_file.object_size();x++){
			if (objects_file.object(x).obj_id() == edit_object->obj_id()){
				//objectsp::object* edit_object_changer = new_objects_file.add_object();
				//edit_object_changer->CopyFrom(*edit_object);
				objects_file.mutable_object(x)->CopyFrom(*edit_object);
				std::cout << "Modified object: " << objects_file.object(x).obj_alias_list() << std::endl;
				log_string = "Modified object: " + objects_file.object(x).obj_alias_list();
				logger(log_string);
			}
			//else {
			//	objectsp::object* edit_object_changer = new_objects_file.add_object();
			//	edit_object_changer->CopyFrom(objects_file.object(x));
			//}
		}
		objects_file.SerializeToOstream(&outfile);
		std::cout << "serialized to " << database_ << std::endl;
		log_string = "Serialized to" + database_;
		logger(log_string);
		edit_ack->set_acknowledge(true);
		outfile.close();

		return grpc::Status::OK;
	}
	else {
		std::cout << "Failed to open file: "<<database_ << std::endl;
		log_string = "Failed to open file: " + database_;
		logger(log_string,2);
		return grpc::Status::CANCELLED;
	}
}

grpc::Status ObjectConnectionImpl::DeleteObject(grpc::ServerContext* context, const objectsp::object_id* object_id, objectsp::Ack* ack) {
	logger("Successfully called DeleteObject!");
	std::string log_string;
	bool object_deleted = false;
	std::ofstream outputfile;
	outputfile.open(database_, std::ios::out|std::ios::binary);
	if (outputfile.good()) {	
		for (int x = 0; x < objects_file.object_size();x++) {
			if (objects_file.object(x).obj_id() == object_id->id()) {
				objects_file.mutable_object(x)->Clear();
				objects_file.mutable_object(x)->set_obj_id(object_id->id());
				objects_file.mutable_object(x)->set_obj_alias_list("deleted");
				std::cout << "Object: " << object_id->id() << " was succesffuly deleted." << std::endl;
				log_string = "Object: " + object_id->id();
				log_string = log_string + " was successfully deleted.";
				logger(log_string);
				object_deleted = true;
				outputfile.clear();
				objects_file.SerializeToOstream(&outputfile);
			}
		}
	}
	if (object_deleted == false) {
		logger("No objects were deleted",1);
		outputfile.close();
		ack->set_acknowledge(false);
		return grpc::Status::CANCELLED;
	}
	else {
		outputfile.close();
		ack->set_acknowledge(true);
		return grpc::Status::OK;
	}
}
grpc::Status ObjectConnectionImpl::CreateObject(grpc::ServerContext* context, const objectsp::object* object,objectsp::object_id* return_id) {
	std::cout << "Successfully called create new object" << std::endl;
	std::string log_string;
	logger("Successfully called CreateObject");
	std::cout << "Current last object ID: " << objects_file.object(objects_file.object_size()-1).obj_id() << std::endl;

	int get_new_id = objects_file.object(objects_file.object_size()-1).obj_id() + 1;
	objectsp::object* new_object = objects_file.add_object();
	new_object->CopyFrom(*object);
	new_object->set_obj_id(get_new_id);
	std::cout << "New object created with ID: " << get_new_id << std::endl;
	log_string = "New object create with ID: " + get_new_id;
	logger(log_string);
	std::ofstream newfile;
	newfile.open(database_,std::ios::out|std::ios::binary);
	if (newfile.good()) {
		std::cout << "Successfully opened: "<<database_ << std::endl;
		objects_file.SerializeToOstream(&newfile);
		newfile.close();
		std::cout << "Successfully Serialized new objects_file" << std::endl;
		log_string = "Successfully serialized new objects file to: " + database_;
		logger(log_string);
		return_id->set_id(get_new_id);
		return grpc::Status::OK;
	}
	else {
		std::cout << "Something went wrong, unable to open file. Aborting process" << std::endl;
		logger("Somthing went wrong trying to create the new object!");
		newfile.close();
		return grpc::Status::CANCELLED;
	}
}

void RunServer (const std::string& db_path, const std::string& host_path) {
    ObjectConnectionImpl service(db_path);
    grpc::ServerBuilder builder;
	std::string log_string;

    builder.AddListeningPort(host_path, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);

    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << host_path << std::endl;
	log_string = "Server started... Listening on: " + host_path;
	logger(log_string);
    server->Wait();
}
