#include <iostream>
#include <fstream>
#include "object_server.h"

int main(int argc, char* argv[]) {
	std::string file_location;
	std::string host_location;
	
	if (argc == 1 || argc == 2){
		std::cout << "To run the server: "
					  << "./object_server.o [proto file name and location] [host ip:port]" << std::endl;
		return 0;
	}

	for(int x = 1; x < argc; x++) {
		if (argv[x] == "help" || argv[x] == "--help")
		{
			std::cout << "To run the server: "
					  << "./object_server.o [proto file name and location] [host ip:port]" << std::endl;
			return 0;
		}
		if (!file_exists(argv[1]))
		{
			
		}
		
		if (x == 1 && file_exists(argv[x])) file_location = argv[x];
		else if (x == 2) host_location = argv[x];
	}
    RunServer(file_location, host_location);
    
    return 0;
}
