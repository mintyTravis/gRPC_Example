#include <iostream>
#include <sys/stat.h>
#include <fstream>
#include <string>
#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>
#include "../protos/mud_objects.grpc.pb.h"

class ObjectConnectionImpl final : public objectsp::MUDCommunication::Service {
    public:
        objectsp::objects objects_file;
		std::string database_;
        explicit ObjectConnectionImpl(const std::string&db);
        grpc::Status QueryForObject(grpc::ServerContext* context,const objectsp::object_id* object_id,objectsp::object* return_object) override;
		grpc::Status QueryAllObjects(grpc::ServerContext* context, const objectsp::Empty* empty_obj,objectsp::objects* return_objects ) override;
		grpc::Status QueryForObjectByType(grpc::ServerContext* context, const objectsp::object_type* objectType, objectsp::objects* return_objects) override;
		grpc::Status QueryForObjectNames(grpc::ServerContext* context, const objectsp::object_name* objectName, objectsp::objects* return_objects) override;
		grpc::Status EditObject(grpc::ServerContext* context, const objectsp::object* edit_object, objectsp::Ack* edit_ack) override;
		grpc::Status DeleteObject(grpc::ServerContext* context, const objectsp::object_id* object_id, objectsp::Ack* ack) override;
		grpc::Status CreateObject(grpc::ServerContext* context, const objectsp::object* object,objectsp::object_id* return_id) override;
		~ObjectConnectionImpl(){};

};

void RunServer(const std::string& db_path, const std::string& host_path);
objectsp::objects parseProtobuff(const std::string& db);

inline bool file_exists(const std::string& name) {
	struct stat buffer;
	return (stat (name.c_str(),&buffer) == 0);
}